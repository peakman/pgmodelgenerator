import XCTest

import PGModelGeneratorTests

var tests = [XCTestCaseEntry]()
tests += PGModelGeneratorTests.allTests()
XCTMain(tests)