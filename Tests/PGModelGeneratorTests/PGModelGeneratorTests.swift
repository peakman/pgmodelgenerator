import XCTest
@testable import PGModelGenerator
import PostgreSC

let dbType = "hellodev"
let testDbname = "hello"

let codeGenDir = URL(fileURLWithPath: #file, isDirectory: false)
  . deletingLastPathComponent()
  . deletingLastPathComponent()
  . deletingLastPathComponent()
  . appendingPathComponent("Tests", isDirectory: true)

class BasicCodeGenTests3: XCTestCase {
    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var result: QueryResult!
    var row: QueryResultRow?
    var spec: String!
    let stringVal = "???"
    let textVal = "some text"
    let int16Val = 2345
    let int32Val : Int? = nil
    let int64Val = Int64(6400)
    let floatDefault : Float = Float32(-99.99)
    let doubleVal : Double = 123.079523
    let boolVal : Bool = true
    let dateVal : Date = BasicCodeGenTests3.dateFormatter!.date(from: "1946-10-24")!
    let tsVal : Date = BasicCodeGenTests3.timestampFormatter!.date(from: "2006-12-19 16:39:57")!
    
    override func setUp() {
        super.setUp()
        print("Code gen dir: ",codeGenDir.path)
        connection = Database.connect(dbType: dbType)
        let createTable =
            "CREATE TABLE spec3 (" +
                "int16_column int2," +
                "int32_column int4," +
                "int64_column int8," +
                "text_column text," +
                "string_column varchar," +
                "single_float_column float4 ," +
                "double_float_column float8," +
                "boolean_column boolean," +
                "date_column date," +
                "timestamp_column timestamp," +
                "raw_byte_column bytea" +
        ");"
        
        _ =  connection.execute(query: Query("DROP TABLE  IF EXISTS spec3;"))
        _ =  connection.execute(query: Query(createTable))
        _ =  connection.execute(query: Query("INSERT INTO spec3 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, null)"),
                                parameters: [nil, int32Val, int64Val, textVal, nil, nil , doubleVal , boolVal, dateVal, tsVal])
        _ =  connection.execute(query: Query("INSERT INTO spec3 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, null)"), parameters: [nil, 32, 64,"text", "string", 99.01, 89.0143, false, Date(), Date() ])
        result = connection.execute(query: "SELECT * FROM spec3 ")
        spec = PGModelGenerator(queryResult: result!,table: "spec3", notNull: [("int16_column", int16Val),("string_column",stringVal)]).createTypedRowSpec(type: "Spec3", dir: codeGenDir)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSimpleCodeGen() {
        XCTAssertNoThrow(PGModelGenerator(queryResult: result).typedRowSpec(type: "Spec"))
    }
    
}
