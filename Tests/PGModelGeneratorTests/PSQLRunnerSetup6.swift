//
//  PSQLRunner.swift
//  PostgreSQLTests
//
//  Created by Steve Clarke on 23/06/2018.
//  Copyright © 2018 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PGModelGenerator
import PostgreSC


class PSQLRunnerSetup4: XCTestCase {

    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var result: QueryResult!
    var row: QueryResultRow?
    var spec: String!
    let stringVal = "abcxyz"
    let textVal = "some text"
    let int16Val = 1600
    let defaultInt16 : Int = 2345
    let int32Val : Int? = nil
    let int64Val = Int64(6400)
    let floatDefault : Float = -99.99
    let defaultFloat : Float = 23.89
    let doubleVal : Double = 123.079523
    let boolVal : Bool = true
    let dateVal : Date = PSQLRunnerSetup4.dateFormatter!.date(from: "1946-10-24")!
    let defaultDate : Date = PSQLRunnerSetup4.dateFormatter!.date(from: "1946-12-17")!
    let tsVal : Date = PSQLRunnerSetup4.timestampFormatter!.date(from: "2006-12-19 16:39:57")!
    let defaultTS : Date = PSQLRunnerSetup4.timestampFormatter!.date(from: "1946-12-17 12:12:12")!
    let rawByteVal = Data(repeatElement(UInt8(15), count: 5))

    override func setUp() {
        super.setUp()
        connection = Database.connect(dbType: dbType)
        let createTable =
            "CREATE TABLE spec4 (" +
                "int16_column int2 default 236 ," +
                "int32_column int4," +
                "int64_column int8 not null," +
                "text_column text," +
                "string_column varchar," +
                "single_float_column float4 ," +
                "double_float_column float8," +
                "boolean_column boolean," +
                "date_column date," +
                "timestamp_column timestamp," +
                "raw_byte_column bytea" +
        ");"
        // E'\\\\x41 62 43 44 45 46'
        _ =  connection.execute(query: Query("DROP TABLE IF EXISTS spec4;"))
        _ =  connection.execute(query: Query(createTable))
        _ =  connection.execute(query: Query("INSERT INTO spec4 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, $11::bytea)"),
                                parameters: [int16Val, int32Val, int64Val, textVal, stringVal, nil , doubleVal , boolVal, dateVal, tsVal, rawByteVal])
        _ =  connection.execute(query: Query("INSERT INTO spec4 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, null)"), parameters: [nil, 32, 64,"text", "string", nil, defaultFloat, false, nil, nil ])
        result = connection.execute(query: "SELECT * FROM spec4 ")
        spec = PGModelGenerator(queryResult: result,table: "spec4", notNull: [
            ("int16_column", defaultInt16),
            ("single_float_column", defaultFloat),
            ("date_column", defaultDate),
            ("timestamp_column", defaultTS)
            ]).createTypedRowSpec(type: "Spec4", dir: codeGenDir)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSimplePSQL() {
        _ = PGModelGenerator(queryResult: result)
        let command = "select count(*) from spec4"
        let output = PGModelGenerator.runPsql(command: command,options:  ["-t", "-A"])
        print("psql output: ",output )
        XCTAssertNoThrow(PGModelGenerator.runPsql(command: command))
        XCTAssertEqual(output, ["2"])
    }
    
    func testTableInfo1() {
        let _ = PGModelGenerator(queryResult: result)
        let output = PGModelGenerator.infoFor(dbname: testDbname, table: "spec4")
        //print("table output: ",output )
        XCTAssertEqual(output[0].column, "int16_column")
        XCTAssertEqual(output[0].nullable, true)
        XCTAssertEqual(output[0].default, "236")
        XCTAssertEqual(output[0].type, .Int16)
    }
    
    func testTableInfo2() {
        let spec =  PGModelGenerator(queryResult: result,table: "spec4", notNull: nil).typedRowSpec(type: "Spec4")
        print(spec)
        XCTAssertTrue(spec.contains("public let int64_column : Int"))
        XCTAssertFalse(spec.contains("public let int64_column : Int?"))
    }
}


class PSQLRunnerSetup6: XCTestCase {
    
    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var result: QueryResult!
    var row: QueryResultRow?
    var spec: String!
    let stringVal = "abcxyz"
    let textVal = "some text"
    let int16Val = 1600
    let defaultInt16 : Int = 2345
    let int32Val : Int? = nil
    let int64Val = Int64(6400)
    let floatDefault : Float = -99.99
    let defaultFloat : Float = 23.89
    let doubleVal : Double = 123.079523
    let boolVal : Bool = true
    let dateVal : Date = PSQLRunnerSetup6.dateFormatter!.date(from: "1946-10-24")!
    let defaultDate : Date = PSQLRunnerSetup6.dateFormatter!.date(from: "1946-12-17")!
    let tsVal : Date = PSQLRunnerSetup6.timestampFormatter!.date(from: "2006-12-19 16:39:57")!
    let defaultTS : Date = PSQLRunnerSetup6.timestampFormatter!.date(from: "1946-12-17 12:12:12")!
    let rawByteVal = Data(repeatElement(UInt8(15), count: 5))
    
    override func setUp() {
        super.setUp()
        connection = Database.connect(dbType: dbType)
        let createTable =
            "CREATE TABLE spec6 (" +
                "int16_column int2  ," +
                "int32_column int4," +
                "int64_column int8 not null," +
                "text_column text," +
                "string_column varchar ," +
                "single_float_column float4 ," +
                "double_float_column float8," +
                "boolean_column boolean," +
                "date_column date," +
                "timestamp_column timestamp," +
                "raw_byte_column bytea," +
                "PRIMARY KEY (int16_column)" +
        ");"
        // E'\\\\x41 62 43 44 45 46'
        _ =  connection.execute(query: Query("DROP TABLE IF EXISTS spec6;"))
        _ =  connection.execute(query: Query(createTable))
        _ =  connection.execute(query: Query("INSERT INTO spec6 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, $11::bytea)"),
                                parameters: [int16Val, int32Val, int64Val, textVal, stringVal, nil , doubleVal , boolVal, dateVal, tsVal, rawByteVal])
        result = connection.execute(query: "SELECT * FROM spec6 ")
        spec = PGModelGenerator(queryResult: result,table: "spec6", notNull: [
            ("int16_column", defaultInt16),
            ("single_float_column", defaultFloat),
            ("date_column", defaultDate),
            ("timestamp_column", defaultTS)
            ]).createTypedRowSpec(type: "Spec6", dir: codeGenDir)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGeneratedSpec() {
        XCTAssertNotNil(spec)
    }
    
 }

