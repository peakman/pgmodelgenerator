import Foundation
import PostgreSC
public final class Spec4 : TypedRow {
	public let int16_column : Int
	public let int32_column : Int?
	public let int64_column : Int
	public let text_column : String?
	public let string_column : String?
	public let single_float_column : Float
	public let double_float_column : Double?
	public let boolean_column : Bool?
	public let date_column : Date
	public let timestamp_column : Date
	public let raw_byte_column : Data?
	public init(row: QueryResultRow) {
		let values : [Any?] = row.columnValues
		var value : Any
		(_, value) = Spec4.checkNotNull(row: row, column: 0, failIfNull: false, defaultValue: Int16( 2345))
		self.int16_column = Int( value as! Int16)
		self.int32_column = values[1] != nil  ? Int(values[1] as! Int32) : nil
		(_, value) = Spec4.checkNotNull(row: row, column: 2, failIfNull: true)
		self.int64_column = Int( value as! Int64)
		self.text_column = values[3] as? String
		self.string_column = values[4] as? String
		(_, value) = Spec4.checkNotNull(row: row, column: 5, failIfNull: false, defaultValue: Float( 23.89))
		self.single_float_column = Float( value as! Float)
		self.double_float_column = values[6] as? Double
		self.boolean_column = values[7] as? Bool
		(_, value) = Spec4.checkNotNull(row: row, column: 8, failIfNull: false, defaultValue: Date(fromPgString: "1946-12-17 00:00:00"))
		self.date_column = Date(fromPgDate: value as! Date)
		(_, value) = Spec4.checkNotNull(row: row, column: 9, failIfNull: false, defaultValue: Date(fromPgString: "1946-12-17 12:12:12"))
		self.timestamp_column = Date(fromPgDate: value as! Date)
		self.raw_byte_column = values[10] as? Data
	}

	public init(int16_column: Int = Int(2345), int32_column: Int?, int64_column: Int, text_column: String?, string_column: String?, single_float_column: Float = Float(23.89), double_float_column: Double?, boolean_column: Bool? = Bool(false), date_column: Date = Date(fromPgString:"1946-12-17 00:00:00"), timestamp_column: Date = Date(fromPgString:"1946-12-17 12:12:12"), raw_byte_column: Data? ) {
		self.int16_column = int16_column
		self.int32_column = int32_column
		self.int64_column = int64_column
		self.text_column = text_column
		self.string_column = string_column
		self.single_float_column = single_float_column
		self.double_float_column = double_float_column
		self.boolean_column = boolean_column
		self.date_column = date_column
		self.timestamp_column = timestamp_column
		self.raw_byte_column = raw_byte_column
	}
}
