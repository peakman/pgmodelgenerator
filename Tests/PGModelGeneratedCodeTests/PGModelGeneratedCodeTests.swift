import XCTest
import PostgreSC
@testable import PGModelGenerator


let dbType = "hellodev"
let testDbname = "hello"

let codeGenDir = URL(fileURLWithPath: #file, isDirectory: false)
    . deletingLastPathComponent()
    . deletingLastPathComponent()
    . deletingLastPathComponent()
    . appendingPathComponent("Tests", isDirectory: true)
let int_16Value = Int(7895)

let spec3Min = Spec3(int16_column: int_16Value, int32_column: nil, int64_column: nil, text_column: nil, string_column: "Spec3 string", single_float_column: nil, double_float_column: nil, boolean_column: nil, date_column: nil, timestamp_column: nil, raw_byte_column: nil)

class BasicTests: XCTestCase {
    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var result: QueryResult!
    var row: QueryResultRow?
    var spec: String!
    var specRow : Spec3!
    var specRow2 : Spec3!
    let stringVal = "???"
    let textVal = "some text"
    let int16Val = 2345
    let int32Val : Int? = nil
    let int64Val = Int64(6400)
    let floatDefault : Float = Float32(-99.99)
    let doubleVal : Double = 123.079523
    let boolVal : Bool = true
    let dateVal : Date = BasicTests.dateFormatter!.date(from: "1946-10-24")!
    let tsVal : Date = BasicTests.timestampFormatter!.date(from: "2006-12-19 16:39:57")!

    override func setUp() {
        connection = Database.connect(dbType: dbType)
        _ =  connection.execute(query: Query("DELETE from spec3;"))
        _ =  connection.execute(query: Query("INSERT INTO spec3 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, null)"),
                                parameters: [nil, int32Val, int64Val, textVal, nil, nil , doubleVal , boolVal, dateVal, tsVal])
        _ =  connection.execute(query: Query("INSERT INTO spec3 values($1::int, $2::int, $3::int, $4, $5, $6::float, $7::float8, $8::boolean,$9::date ,$10::timestamp, null)"), parameters: [nil, 32, 64,"text", "string", 99.01, 89.0143, false, Date(), Date() ])
        result = connection.execute(query: "SELECT * FROM spec3 ")
        specRow = Spec3(row: result.readResultRowAtIndex(rowIndex: 0))   //Uses default values for unexpceted nulls
        specRow2 = Spec3(row: result.readResultRowAtIndex(rowIndex: 1))   //Uses default values for unexpceted nulls
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testSpec3Int16() {
        XCTAssertEqual(spec3Min.int16_column, int_16Value)
    }

    func testGetSingleFloat() {
        XCTAssertEqual(specRow.single_float_column,nil)
    }
    
    func testGetDate() {
        XCTAssertEqual(specRow.date_column!,dateVal)
    }
    
    func testGetTimestamp() {
        XCTAssertEqual(specRow.timestamp_column!,tsVal)
    }
    
    func testGetDoubleFloat() {
        XCTAssertEqual(specRow.double_float_column!,doubleVal)
    }
    
    func testGetInt16() {
        XCTAssertEqual(specRow.int16_column,int16Val)
    }
    
    func testGetInt32Null() {
        XCTAssertEqual(specRow.int32_column,int32Val)
    }
    
    func testGetText() {
        XCTAssertEqual(specRow.text_column!,textVal)
    }
    
    func testGetString() {
        XCTAssertEqual(specRow.string_column,stringVal)
    }
    
    func testGetNullInt16() {
        //XCTAssertThrowsError(Spec2(row: result.readResultRowAtIndex(rowIndex: 1)))
        //Fatal error stops the test.
    }
    
}
