import Foundation
import PostgreSC
public final class Spec3 : TypedRow {
	public let int16_column : Int
	public let int32_column : Int?
	public let int64_column : Int?
	public let text_column : String?
	public let string_column : String
	public let single_float_column : Float?
	public let double_float_column : Double?
	public let boolean_column : Bool?
	public let date_column : Date?
	public let timestamp_column : Date?
	public let raw_byte_column : Data?
	public init(row: QueryResultRow) {
		let values : [Any?] = row.columnValues
		var value : Any
		(_, value) = Spec3.checkNotNull(row: row, column: 0, failIfNull: false, defaultValue: Int16( 2345))
		self.int16_column = Int( value as! Int16)
		self.int32_column = values[1] != nil  ? Int(values[1] as! Int32) : nil
		self.int64_column = values[2] != nil  ? Int(values[2] as! Int64) : nil
		self.text_column = values[3] as? String
		(_, value) = Spec3.checkNotNull(row: row, column: 4, failIfNull: false, defaultValue: String( "???"))
		self.string_column = String( value as! String)
		self.single_float_column = values[5] as? Float
		self.double_float_column = values[6] as? Double
		self.boolean_column = values[7] as? Bool
		self.date_column = values[8] as? Date
		self.timestamp_column = values[9] as? Date
		self.raw_byte_column = values[10] as? Data
	}

	public init(int16_column: Int = Int(2345), int32_column: Int?, int64_column: Int?, text_column: String?, string_column: String = String("???"), single_float_column: Float?, double_float_column: Double?, boolean_column: Bool? = Bool(false), date_column: Date?, timestamp_column: Date?, raw_byte_column: Data? ) {
		self.int16_column = int16_column
		self.int32_column = int32_column
		self.int64_column = int64_column
		self.text_column = text_column
		self.string_column = string_column
		self.single_float_column = single_float_column
		self.double_float_column = double_float_column
		self.boolean_column = boolean_column
		self.date_column = date_column
		self.timestamp_column = timestamp_column
		self.raw_byte_column = raw_byte_column
	}
}
