//
//  CodeGenerator.swift
//  Postgres
//
//  Created by Steve Clarke on 10/06/2018.
//  Copyright © 2018 Steve Clarke. All rights reserved.
//

import Foundation
import libpq
import PostgreSC



let queryPlaceHolder = "__$$__"

public class PGModelGenerator : PsqlRunner {
    unowned let qRes : QueryResult
    let dbname : String
    let tableName : String?
    let notNull: [String]?
    let defaultValues : [Parameter?]?
    
    public init(queryResult: QueryResult, table: String? = nil, notNull: [BareNotNullSpec]? = nil, dbname: String = "hello") {
        self.qRes = queryResult
        self.tableName = table
        self.dbname = dbname
        self.notNull = notNull?.map() {$0.0}
        self.defaultValues = notNull?.map() {
            if let v = $0.1 {
                return v
            } else {
                return nil
            }
        }
    }
    
    static func defaultDirFrom(filePath: String, levelsUp: UInt) -> URL {
        var adjustedURL = URL(fileURLWithPath: filePath)
        for _ in 0..<levelsUp {adjustedURL = adjustedURL.deletingLastPathComponent()}
        print(adjustedURL.path)
        if #available(OSX 10.11, *) {
            return URL(fileURLWithPath: "PGGeneratedCode", isDirectory: true, relativeTo: adjustedURL)
        } else {
            fatalError("Wrong OS version")
        }
    }

    public static func generateDbStuff(parms: [TypedRowParms], connection: Connection, location: String, levelsUp : UInt = 2, dbname: String = "hello") {
        print("Generating DB-related source")
        var isDir: ObjCBool = ObjCBool(false)
        FileManager.default.fileExists(atPath: location, isDirectory: &isDir)
        let dir : URL
        if isDir.boolValue {
            dir = URL(fileURLWithPath: location, isDirectory: true)
        } else {
            dir = self.defaultDirFrom(filePath: location, levelsUp: levelsUp)
        }
        for qn in parms {
            let result  = connection.execute(query: qn.query)
            let codeGen = PGModelGenerator(queryResult: result, table: qn.table, notNull: qn.notNull, dbname: dbname )
            #if !QUIET
                print(codeGen.createTypedRowSpec(type: qn.type , dir: dir ))
            #endif
        }
    }
    
    public func createTypedRowSpec(type: String, dir: URL) -> String {
        let source =  typedRowSpec(type: type)
        let fileURL = dir.appendingPathComponent(type).appendingPathExtension("swift")
        do {
            try source.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch {
            fatalError("Unable to write to \(fileURL)")
        }
        return source
    }
    
    public func typedRowSpec(type: String) -> String {
        let keyTypes : [ColumnType] = [.Int16, .Int32, .Int64, .UUID]
        let intTypes : [ColumnType] = [.Int16, .Int32, .Int64]
        var tableInfo : [TableInfo]? = nil
        let primaryKey : PrimaryKey?
        let autoIncrements : Bool?
        var source : [String] = ["import Foundation", "import PostgreSC"]

        func protocolType(primaryKey: PrimaryKey?, intTypes: [ColumnType]) -> String {
            if primaryKey != nil && primaryKey!.columns.count == 1 && keyTypes.contains(primaryKey!.pgTypes[0]) {
                return String("KeyedTypedRow")
            } else {
                return String(describing: TypedRow.self)
            }
        }
        
        func quotedDefault(parm: Parameter?) -> Parameter? {
            if let p = parm {
                return "\"\(p.asPgString())\""
            } else {
                return nil
            }
        }
        func setInstanceFromCode(col: Int, deflt: Any?,name: String, sType: Any.Type ,
                                 dbType: Any.Type, ctor:(String,String), optional: Bool,
                                 primaryKey: PrimaryKey?, keyAutoIncrements: Bool? )  -> String {
            var source = ""
            if !(primaryKey != nil && (name == primaryKey!.columns[0]) && keyAutoIncrements == true ) {
                source.append("\t\tself.\(name) = \(name)")
            }
            return source
        }
        
        func setInstanceFromQueryResult(col: Int, deflt: Any?,name: String, sType: Any.Type ,
                                        dbType: Any.Type, ctor:(String,String), optional: Bool,
                                        primaryKey: PrimaryKey?, keyAutoIncrements: Bool? ) -> String {
            var source = ""
            let cast : String = optional ? "as?" : "as!"
            if !(primaryKey != nil && name == primaryKey!.columns[0] && keyAutoIncrements == true ) {
                if optional{
                    if sType == dbType {
                        source.append("\t\tself.\(name) = values[\(col)] \(cast) \(dbType)")
                    } else {
                        source.append("\t\tself.\(name) = values[\(col)] != nil  ? \(sType)(values[\(col)] as! \(dbType)) : nil")
                    }
                } else {
                    if deflt == nil {
                        source.append("\t\t(_, value) = \(type).checkNotNull(row: row, column: \(col), failIfNull: true)\n")
                    } else {
                        source.append("\t\t(_, value) = \(type).checkNotNull(row: row, column: \(col), failIfNull: false, defaultValue: \(dbType)(\(ctor.0) \(deflt!)))\n")
                    }
                    source.append("\t\tself.\(name) = \(sType)(\(ctor.1) value as! \(dbType))")
                }
            }
            return source
        }
        
        func initSignature(names: [String], sTypes: [Any.Type], dbTypes: [Any.Type],
                           defaults: [Any?], optionals: [Bool], ctor: [(String, String)],primaryKey: PrimaryKey?, keyAutoIncrements: Bool?) -> String {
            var source : String = ""
            for prop in 0..<names.count {
                let comma = (prop == names.count - 1) ? "" : ","
                let optQ = optionals[prop] ? "?" : ""
                let deflt = (defaults[prop] == nil) ? "" : " = \(sTypes[prop])(\(ctor[prop].0)\(defaults[prop]!))"
                if !(primaryKey != nil && (names[prop] == primaryKey!.columns[0]) && keyAutoIncrements == true) {
                    source.append("\(names[prop]): \(sTypes[prop])\(optQ)\(deflt)\(comma) ")
                }
            }
            return source
        }
        
        func gatherMeta(col: Int, names: inout [String], sTypes: inout [Any.Type], dbTypes:inout [Any.Type],
                        defaults: inout [Any?], optionals: inout [Bool], ctorParms: inout [(String, String)],
                        colTypes: inout [ColumnType],primaryKey: PrimaryKey?, keyAutoIncrements: Bool?) -> String {
            guard let colType = self.qRes.typesForColumnIndexes[col] else  {
                fatalError("\n\nUnknown type \(String(describing: self.qRes.typesForColumnIndexes[col]))\nfor column \(String(cString: PQfname(self.qRes.resultPointer, Int32(col))))\n\n")
            }
            colTypes.append(colType)
            let name = String(cString: PQfname(self.qRes.resultPointer, Int32(col)))
            let parmIndex : Int? = self.notNull?.index(of: name)
            let parmNotNull : Bool = parmIndex != nil
            let tableNotNull : Bool = (tableInfo?.first() { $0.column == name}?.nullable) == false
            let optional : Bool = !(parmNotNull || tableNotNull)
            var ctorParm : (String,String) = ("", "")
            optionals.append(optional)
            names.append(name)
            let parmDefault : Parameter?
            if let pi = parmIndex { parmDefault = self.defaultValues![pi]} else {parmDefault = nil}
            let sType : Any.Type
            let dbType  : Any.Type
            let deflt: Any?
            switch colType {
            case .Int16: sType = Int.self; dbType = Int16.self; deflt = parmDefault
            case .Int32: sType = Int.self; dbType = Int32.self; deflt = parmDefault
            case .Int64: sType = Int.self; dbType = Int64.self; deflt = parmDefault
            case .Boolean: sType = Bool.self; dbType = sType; deflt = parmDefault ?? false
            case .Timestamp: sType = Date.self; dbType = sType; deflt = quotedDefault(parm: parmDefault); ctorParm = ("fromPgString:","fromPgDate:")
            case .Date: sType = Date.self; dbType = sType; deflt = quotedDefault(parm: parmDefault); ctorParm = ("fromPgString:","fromPgDate:")
            case .SingleFloat: sType = Float.self; dbType = Float32.self; deflt = parmDefault
            case .DoubleFloat: sType = Double.self; dbType = Float64.self; deflt = parmDefault
            case .String: sType = String.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .UUID: sType = UUID.self; dbType = UUID.self; deflt =  parmDefault
            case .Text: sType = String.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .Bytes: sType = Data.self; dbType = sType; deflt = parmDefault
            }
            defaults.append(deflt)
            sTypes.append(sType)
            dbTypes.append(dbType)
            ctorParms.append(ctorParm)
            if primaryKey != nil && keyAutoIncrements != nil && name == primaryKey?.columns[0] {
                return ("\tpublic let \(name) : \(sType)\(keyAutoIncrements! ? "? = nil" : "")  ")
            } else {
                return ("\tpublic let \(name) : \(sType)\(optional ? "?" : "")")
            }
        }

        func insertConformance(table: String, names: [String], sTypes: [Any.Type], dbTypeStrings: [String],
                               defaults: [Any?], optionals: [Bool], ctor: [(String, String)],
                               primaryKey: PrimaryKey?, keyAutoIncrements: Bool?) -> String {
            let returnClause: String
            if primaryKey != nil {
                returnClause = "returning \(primaryKey!.columns[0]) as inserted_id"
            } else {
                returnClause = ""
            }
            func columnNames() -> String {
                var source : [String] = []
                for col in names {
                    if primaryKey == nil || col != primaryKey!.columns[0] {
                        source.append(col)
                    } else {
                        if keyAutoIncrements != true {
                            source.append(col)
                        }
                    }
                }
                return source.joined(separator: ",")
            }
            func itemsForInsert() -> [(String, String)] {
                var cols : [(String, String)] = []
                for c in 0..<names.count {
                    cols.append((names[c], dbTypeStrings[c]))
                }
                if keyAutoIncrements == true {
                    cols = cols.filter() {$0.0 != primaryKey!.columns[0]}
                }
                return cols
            }
            func params() -> String {
                var parms : [String] = []
                let items = itemsForInsert()
                for i in 0..<items.count {
                    parms.append( "$\(i+1)::\(items[i].1)")
                }
                return parms.joined(separator: ",")
            }
            var source = ""
            source.append("extension \(type) : PgInsert {\n")
            source.append("\tpublic static var tableName: String = \"\(table)\"\n")
            source.append("\tpublic static var columnsForInsert : [String] = \(itemsForInsert().map {$0.0})\n")
            source.append("\t// queryPlaceHolder gets replaced by columnsForInsert\n")
            source.append("\tpublic static var insertQuery : String = \"insert into \(table) (\(queryPlaceHolder)) values(\(params())) \(returnClause);\"\n")
            source.append("}\n")
            return source
        }
        func insertParmSource(names: [String], pKey: PrimaryKey?, omitKey: Bool?) -> String {
            let insertNames : [String]
            if omitKey == true {
                insertNames = names.filter {$0 != pKey?.columns[0]}
            } else {
                insertNames = names
            }
            return  "\tpublic var insertParms : [Parameter?] {return [\(insertNames.joined(separator: ","))]}\n"
        }
        
        // MARK: Mainline for typedRowSpec starts here
        
        if let tabName = tableName {
            tableInfo = PGModelGenerator.infoFor(dbname: dbname, table: tabName)
            primaryKey = PGModelGenerator.primaryKeyInfo(dbname: dbname, table: tabName, tableInf: tableInfo)
            autoIncrements = PGModelGenerator.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKey)
        } else {
            primaryKey = nil
            autoIncrements = nil
        }
        source.append("public final class \(type) : \(protocolType(primaryKey: primaryKey, intTypes: intTypes)) {")
        var sTypes : [Any.Type] = []
        var colTypes : [ColumnType] = []
        var dbTypes : [Any.Type]  = []
        var names : [String] = []
        var optionals : [Bool] = []
        var ctorParms : [(String,String)] = []
        var defaults : [Any?] = []
        
        for col in 0..<qRes.numberOfColumns {
            source.append(gatherMeta(col: col, names: &names, sTypes: &sTypes, dbTypes: &dbTypes,
                       defaults: &defaults, optionals: &optionals, ctorParms: &ctorParms,
                       colTypes: &colTypes, primaryKey: primaryKey,
                       keyAutoIncrements: PGModelGenerator.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKey)))
        }
        if let pkey = primaryKey {
            source.append("\tpublic var primaryKey : \(sTypes[0])\(autoIncrements == true ? "?" : "") {return \(pkey.columns[0])}")
        }
        if tableName != nil {
            //source.append(insertParmSource(names: names, pKey: primaryKey, omitKey: autoIncrements ))
        }
        source.append("\tpublic init(row: QueryResultRow) {")
        if optionals.contains(true) {source.append("\t\tlet values : [Any?] = row.columnValues")}
        if optionals.contains(false) {source.append("\t\tvar value : Any")}
        for col in 0..<qRes.numberOfColumns {
            source.append(setInstanceFromQueryResult(col: col, deflt: defaults[col],
                                       name: names[col], sType: sTypes[col] , dbType: dbTypes[col],
                                       ctor: ctorParms[col], optional: optionals[col], primaryKey: primaryKey,
                                       keyAutoIncrements: autoIncrements ))
        }
        source.append("\t}\n")
        if tableName != nil {
            let sigsource = initSignature(names: names, sTypes: sTypes, dbTypes: dbTypes, defaults: defaults, optionals: optionals, ctor: ctorParms, primaryKey: primaryKey, keyAutoIncrements: PGModelGenerator.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKey))
            source.append("\tpublic init(\(sigsource)) {")
            for col in 0..<qRes.numberOfColumns {
                source.append(setInstanceFromCode(col: col, deflt: defaults[col],
                                           name: names[col], sType: sTypes[col] , dbType: dbTypes[col],
                                           ctor: ctorParms[col], optional: optionals[col], primaryKey: primaryKey, keyAutoIncrements: autoIncrements) )
            }
            source.append("\t}")
        }
        source.append("}\n")
/*
        source.append("extension \(type) : Codable {}\n")
        if let tabName = tableName {
            let dbts = tableInfo!.map {$0.dbType}

            source.append(insertConformance(table: tabName, names: names, sTypes: sTypes,
                                            dbTypeStrings: dbts, defaults: defaults, optionals: optionals, ctor: ctorParms,
                                            primaryKey: primaryKey,
                                            keyAutoIncrements: autoIncrements))
        }
*/
        return source.joined(separator: "\n")
    }
    
}



